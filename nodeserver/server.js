/*const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.send("../SDK/miner_raw/mine.html");
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});*/
const path = require('path');
const express = require('express')
const app = express()
const port = 3000
const axios = require('axios');

app.use('/js', express.static(path.resolve(__dirname, "../js")));
app.use('/assets', express.static(path.resolve(__dirname, "../assets")));
app.use('/fonts', express.static(path.resolve(__dirname, "../fonts")));
app.get('/', (req, res) => res.sendFile(path.resolve(__dirname,"../miner.html")));

app.get('/stats', (req, res) => {
  axios.get('http://192.168.20.59:8892/stats')
  .then(resp => {
    res.send(resp.data);
  })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))