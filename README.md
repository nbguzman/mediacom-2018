*Using https://github.com/cazala/coin-hive-stratum for custom pool

*Using https://coinhive.com/documentation/miner for web mining library

*Using https://mymonero.com/#/ for address

To run:
1. coin-hive-stratum 8892 --host=pool.supportxmr.com --port=3333
2. Open the miner.html and make sure the ws configuration is using same port as above